# OpenML dataset: iq_brain_size

https://www.openml.org/d/483

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Relationship between IQ and Brain Size

Summary:
Monozygotic twins share numerous physical, psychological, and pathological traits.  Recent advances in in vivo brain image acquisition and analysis have made it possible to determine quantitatively whether: 1) twins share neuroanatomical traits; and 2) neuroanatomical measures correlate with brain size.
Using magnetic resonance imaging and computer-based image analysis techniques, measurements of the volume of the forebrain, the surface area of the cerebral cortex and the mid-sagittal area of the corpus callosum were obtained in 10 pairs of monozygotic twins.  Head circumference, body weight, and Full-Scale IQ were also measured.  Analyses of variance were carried out using genotype, birth order, and sex, as between-subject factors.  Pearson correlation coefficients were computed to assess the interrelationships between brain measures, head circumference, and IQ.
Effects of genotype (but not of birth order) were found for total forebrain volume, total cortical surface area, and callosal area.  Consistent with previous twin studies, highly significant effects of genotype but not birth order were also found for head circumference, body weight, and Full-Scale IQ.  The significant effect of genotype on all measures was not attributable to sex differences across unrelated twin pairs.  Significant correlations were observed between forebrain volume, cortical surface area, and callosal area as well as between each brain measure and head circumference.  No correlation between IQ and any other measure was found.
Monozygotic twins share similarities in forebrain volume, cortical surface area, and callosal area.  Brain measures are highly correlated with one another and with head circumference, but none is correlated with IQ.

Authorization: Contact Authors

Reference:
Tramo MJ, Loftus WC, Green RL, Stukel TA, Weaver JB, Gazzaniga MS.  Brain Size, Head Size, and IQ in Monozygotic Twins.  Neurology  1998; 50:1246-1252.

Description:  This datafile contains 20 observations (10 pairs of twins) on 9 variables.  This data set can be used to demonstrate simple linear regression and correlation.


Variable Names in order from left to right:
CCMIDSA: Corpus Collasum Surface Area (cm2)
FIQ: Full-Scale IQ
HC: Head Circumference (cm)
ORDER: Birth Order
PAIR: Pair ID (Genotype)
SEX: Sex (1=Male 2=Female)
TOTSA: Total Surface Area (cm2)
TOTVOL: Total Brain Volume (cm3)
WEIGHT: Body Weight (kg)



Therese Stukel
Dartmouth Hitchcock Medical Center
One Medical Center Dr.
Lebanon, NH 03756
e-mail: stukel@dartmouth.edu


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: 2

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/483) of an [OpenML dataset](https://www.openml.org/d/483). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/483/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/483/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/483/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

